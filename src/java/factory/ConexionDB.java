package factory;

import java.sql.*;//Importar todas las librerias de sql
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ConexionDB {
    protected String[] parametros; //Array que recibe los parametros de la conexion
    protected Connection conexion;
    //El siguiente método abstracto no se implementa solamente se declara, se
    //implementara en la subclase
    
    abstract Connection open(); //Método abstracto que devulve un objeto connection 
    
    //Crear métodos para ejecutar las consultas (Instrucciones DML del tipo Select )
    public ResultSet consultaSQl(String consulta)
    {
        ResultSet rs = null; //Tabla temporal donde se almacenan los datos de nuestra consulta
        Statement st; //Objeto Statement es el encargado de ejecutar las consultas
        
        //Inicio de try catch para manejo de excepciones SQL
        try
        {
            st = conexion.createStatement();
            rs = st.executeQuery(consulta); //Se ejecuta la consulta, asignandose como paramentro al metodo executeQuery() del objeto statement
            
        } catch (SQLException ex) {
            ex.printStackTrace(); //Utilizado para mostrar el nombre de la excepcion junto a su mensaje
        }
        
        //Devolvemos nuestra tabla temporal
        return rs;
    }
    
    
    //Método para ejecutar consultas DML(Insert, Update, Delete)
    public boolean ejecutarSQl(String consulta)
    {
        Statement st ;//Objeto statement es el encardao de ejecutar las consultas
        boolean guardar = true;
        //Inicio de try catch para manejo de excepciones SQL
        try
        {
            st= conexion.createStatement();
            st.executeUpdate(consulta);
        }catch (SQLException ex) {
            guardar = false;
            ex.printStackTrace(); //Utilizado para mostrar el nombre de la excepcion junto a su mensaje
        }
        
        //Devolvemos true luego de ejecutar con exito nuestra consulta
        return guardar;
    }
    
    //Creamos método para cerrar la conexión
    public boolean cerrarConexion()
    {
        boolean ok = true;
        try
        {
            conexion.close();
        } catch (Exception ex) {
            ok = false;
            ex.printStackTrace(); //Utilizado para mostrar el nombre de la excepcion junto a su mensaje
        }
        
        //Devolvemos true luego de cerrar la conexión
        return ok;
    }
}

