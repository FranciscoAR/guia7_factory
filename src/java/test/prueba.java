package test;

import DAO.*;
import model.Categoria;
import java.util.*;

public class prueba {
    public static void main(String[] args) {
        prueba evaluar = new prueba();
        System.out.println("Archivo de pruebas unitarias ");
        //evaluar.guardarCategoria();
        evaluar.listarCategorias();
        evaluar.borrarCategoria();
        evaluar.listarCategorias();
        //evaluar.EditarCategoria();
    }
    
    public void listarCategorias()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        List<Categoria> listar = categoria.Listar();
        System.out.println("LISTADO DE CATEGORIAS");
        for(Categoria categoriaListar : listar)
        {
            System.out.println("ID: " + categoriaListar.getId_categoria() + 
                    " NOMBRE: " + categoriaListar.getNom_categoria() +
                    " ESTADO: " + categoriaListar.getEstado_categoria());
        }
    }
    
    public void EditarCategoria()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        Categoria cat_edit = categoria.editarCat(1000);
        System.out.println("CATEGORIA A MODIFICAR");
        System.out.println("ID: " + cat_edit.getId_categoria() +
                "NOMBRE: " + cat_edit.getNom_categoria() +
                "ESTADO: " + cat_edit.getEstado_categoria());
    }
    
    public void guardarCategoria(){
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        Categoria guardar_cat = new Categoria();
        guardar_cat.setId_categoria(1010);
        guardar_cat.setNom_categoria("Muebles Hogareños");
        guardar_cat.setEstado_categoria(1);
        categoria.guardarCat(guardar_cat);
    }
    public void borrarCategoria()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        categoria.borrarCat(1009);
    }
}
