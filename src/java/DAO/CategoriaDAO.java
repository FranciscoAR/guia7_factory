package DAO;

import model.Categoria;
import java.util.*;

public interface CategoriaDAO { //DAO significa Data acces Object
    /*
    Definir los métodos, como la clase en interface los métodos no se implementan
    aqui, los métodos son de tipo abstractos
    */
    public List<Categoria> Listar();
    public List<Categoria> Listar2();
    public Categoria editarCat(int id_cat_edit);
    public boolean guardarCat(Categoria categoria);
    public boolean borrarCat(int id_cat_borrar);
}
