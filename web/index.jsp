<%-- 
    Document   : index
    Created on : 10 jun. 2022, 16:50:18
    Author     : franc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            Inicio
        </title>
        <%@include file = "WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body>
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <h3>Bienvenid@!</h3>
        
        <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="WEB-INF/vistas-parciales/pie.jspf" %>
    </body>
</html>
