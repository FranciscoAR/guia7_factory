<%@page import="model.Categoria"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la session en el controlador -->
<jsp:useBean id="listaCategorias" scope="session" class="java.util.List" />
<html>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <title>
               Control de inventario
           </title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body>
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <h2 align="center">Bienvenid@!</h2>
        <div style="width: 600px">
            <a href="<%= request.getContextPath() %>/categorias.go?opcion=crear" class="btn btn-success btn-sm glyphicon glyphicon-pencil" role="button"> Nueva Categoria</a>
            <h3>Listado de Categorias Registradas</h3>
            <table class="table table-striped">
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        NOMBRE
                    </th>
                    <th>
                        ESTADO
                    </th>
                    <th>
                        ACCIÓN
                    </th>
                </tr>
                
                    <%
                    for(int i = 0; i < listaCategorias.size(); i++)
                    {
                    Categoria categoria = new Categoria();
                    categoria = (Categoria)listaCategorias.get(i);
                    
                    %>
                    <tr>
                        <td><%= categoria.getId_categoria()%></td>
                        <td><%= categoria.getNom_categoria()%></td>
                        <td><%= categoria.getEstado_categoria()%></td>
                        <td>
                            <a href="<%= request.getContextPath() %>/categorias.go?opcion=editar&id=<%= categoria.getId_categoria()%>" class="btn btn-info btn-sm glyphicon glyphicon-edit" role="button">Editar</a>
                            <a href="<%= request.getContextPath() %>/categorias.go?opcion=eliminar&id=<%= categoria.getId_categoria()%>" class="btn btn-danger btn-sm glyphicon glyphicon-remove" role="button">Eliminar</a>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    
            </table>
        </div>
        
        <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
    </body>
</html>

