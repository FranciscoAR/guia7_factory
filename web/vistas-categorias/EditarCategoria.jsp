<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Para importar los datos de la categoria a editar -->
<jsp:useBean id="editarCategoria" scope="session" class="model.Categoria" />
<!DOCTYPE html>
<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            Control de Inventario
        </title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
        <script type="text/javascript">
            function regresar(url){ //Función para el botón regresar
                location.href = url;
            }
        </script>
    </head>
    <body>
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <h3>Mantenimiento Categorias</h3>
        
        <form class="form-horizontal" id="formCategoria" name="frmCategoria" action="<%= request.getContextPath() %>/categorias.go" method="POST">
            <input type="hidden" name="id_categoria" value="<%= editarCategoria.getId_categoria() %>" >
            <div class="form-group">
                <label for="txtNomCategoria" class="col-sm-2 control-label">Nombre:</label> 
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtNomCategoria" value="<%= editarCategoria.getNom_categoria() %>" >
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstadoCategoria" class="col-sm-2 control-label" >Estado:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtEstadoCategoria" value="<%= editarCategoria.getEstado_categoria() %>" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-success btn-sm" name="btnEditar" value="Editar" />
                    <input type="button" class="btn btn-danger btn-sm" onclick="regresar('<%= request.getContextPath() %>/categorias.go?opcion=listar')"
                           name="btnRegresar" value="Regresar" />
                </div>
            </div>
        </form>
        
        <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
    </body>
</html>
